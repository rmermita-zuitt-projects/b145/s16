console.log('Sup cuz?!')

//[Section 1] Assignment Operators


//1. Basic assignment operator (=)
//This allows us to add the value or the right operand to a variable and assigns the result to the variable.
let assignmentNumber = 5;

let message = 'This is an assignment operator sample usage';

//2. Addition assignment operator (+=)
//The addition assignment operator adds the value of the right operand to a variable and assigns the result to the variable.
// assignmentNumber = assignmentNumber + 2;
console.log("Result of the operation: " + assignmentNumber);

//shorthanded version
assignmentNumber += 2;
console.log("Result of the operation: " + assignmentNumber);

	//[Section 1.2] Arithmetic Operators
	//(+, -, *, /)
//3. Subtraction assignment operators (-=)
assignmentNumber -= 3;
console.log("Result of the operation: " + assignmentNumber);
//4. Multiplication assignment operators (*=)
assignmentNumber *= 6;
console.log("Result of the operation: " + assignmentNumber);
//5. Division assignment operators (/=)
assignmentNumber /= 3;
console.log("Result of the operation: " + assignmentNumber);

let value = 8;

//Addition assignment (+=)
//value += 15;

//Subtraction assignment (-=)
//value -= 5;

//Multiplication assignment (*=)
//value *= 2;

//Division assignment (/=)
value /= 2;

console.log(value);
//[Section] Arithmetic Operators

let x = 15;
let y = 10;

//Addition (+)

let sum = x + y;
console.log(sum);

//Subtraction (-)
let difference = x - y;
console.log(difference);

//Multiplication (*)
let product = x * y;
console.log(product);

//Division (/)
let quotient = x / y;
console.log(quotient);

//Remainder between 2 values (Modulus '%')
let remainder = x % y;
console.log(remainder);

//[Subsection] Multiple Operators and Parentheses

//When multiple operators are applied in a single statement, it follows the PEMDAS rule.

let mdas = 1 + 2 - 3 * 4 / 5;
console.log(mdas);

//The operations were done in the following order to get the final answer.

//Note: The order of operations can be changed by adding parenthesis to the logic.

let pemdas = 1 + (2 - 3) * (4 / 5);
console.log(pemdas);

//By adding parenthesis '()', the order of the operations are changed prioritizing the operations enclosed within parenthesis.

//[Section] Increment and Decrement

let z = 1;

//Pre and Post
//Increment (++)

//Pre-increment (syntax: ++variable)
let preIncrement = ++z;
console.log(preIncrement); //Result of the pre-increment

console.log(z);

//Post-increment (syntax: variable++)
postIncrement = z++;

//The value of z is returned and stored inside the variable called post increment. The value of z is at 2 before it was incremented.
console.log(postIncrement);

console.log(z);


//Decrement (--)
//Pre-decrement (syntax: --variable)
let preDecrement = --z;
console.log(preDecrement);

//Post-decrement (syntax: variable--)
let postDecrement = z--;
console.log(postDecrement);

console.log(z);

//[Section] Type Coercion

let numberA = 6;
let numberB = '6';

//lets check the data types of the values above
//typeof expression - will allows us to identify the data type of a certain value/component.
console.log(typeof numberA);
console.log(typeof numberB);
//number + string /the number data type was converted into a string to perform concatenation instead of addition.
let coercion = numberA + numberB;
console.log(coercion);

//Adding a number and boolean
let expressionC = 10 + true;
console.log(expressionC);

let a = true;
console.log(typeof a);
let b = 10;
console.log(typeof b);

//Note: The boolean value of "true" is also associated with the value of 1.

let expressionD = 10 + false;
console.log(expressionD);

let expressionE = true + false;
console.log(expressionE);

//NUmber with a null value.
let expressionF = 8 + null;
console.log(expressionF);

let d = null;
console.log(typeof d);

//Conversion Rules:
//1. If at least one operand is an object, it will be converted to a primitive value/data type.
//2. After conversion, if at least one operand is a string data type, the second operand is converted to another string to perform concatenation.
//3. In other cases where both operands are converted to numbers then an arithmetic operation is executed.

//The object data type which is "null" operand was converted into a primitive data type.

//String with a null data type.
let expressionG = "Batch145" + null;
console.log(expressionG);

//Number with undefined
expressionH = 9 + undefined;
console.log(expressionH);
let e = undefined;
console.log(typeof e);

//1. Undefined was converted into a number data type NaN
//2. 9 + NaN = NaN

//[Section] Comparison Operators
let name = 'Juan'
	//[Subsection] Equality Operators attempts to convert and compare with 2 different data types.
	//Returns a boolean value
	console.log(1 == 1);
	console.log(1 == 2);
	console.log(1 == '1');
	console.log(1 == true);
	console.log(1 == false);
	console.log(name == 'Juan');
	console.log('Juan' == 'juan');
	// console.log('Juan' == Juan); //this is an error because the variable isn't declared yet.

	//[Subsection] Inequality Operator (!=)
	//Checks whether the operands are not equal/have different values
	console.log(1 != 1);
	console.log(1 != 2);
	console.log(1 != '1');
	console.log(0 != false);
	let juan = 'juan';
	console.log('juan' != juan);

	//[Subsection] "Strict" Equality Operators (===)
	//Checks whether the operands are equal or have the same value.
	//Also compares if the data types are the same.

	console.log(1 === 1);
	console.log(1 === '1');
	console.log(0 === false);

	//[Subsection] "Strict" Inequality Operator (!==)
	//This will check if the operands are not equal/have different values/content
	//Checks both values and data types of both components/operands.
	console.log(1 !== 1);
	console.log(1 !== 2);
	console.log(1 !== '1');
	console.log(0 !== false);

	//Developer's Tip: Upon creating conditions or statement, it is strongly recommended to use "strict" equality operators over "loose" equality operators, because it will be easier for us to pre-determine outcome and results in any given scenario.


//[Section] Relational Operators
let priceA = 1800;
let priceB = 1450;

//Lesser than operator
console.log(priceA < priceB);

//Greater than operator
console.log(priceA > priceB);

let expressionI = 150 <= 150;
console.log(expressionI);

//Developer's Tip: When writing down/selecting variables name that would describe/contain a boolean value. It is writing convention for developers to add a prefix of "is" and "are" together with the variable name to form a variable similar on how to answer a simple yes or no question.

isLegalAge = true;
isRegistered = false;

//For the person to be able to vote, both requirements has to be met.

//We need to use the proper logical operator.
//AND (&&) all criteria has to be met
let isAllowedToVote = isLegalAge && isRegistered;
console.log('Is the person allowed to vote? ' + isAllowedToVote);

//OR (||) at least one criteria has to be met, in order to pass.
let isVaccinated = isLegalAge || isRegistered;
console.log('Did the person pass? ' + isVaccinated);

//NOT (! - Exclamation Point) Operator
	//This will convert/return the opposite value.
	let isTaken = true;
	let isTalented = false;

	console.log(!isTaken);
	console.log(!isTalented);