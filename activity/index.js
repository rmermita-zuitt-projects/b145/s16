console.log('Hello World!')

let x = 35;
let y = 49;

let sum = x + y;
console.log('The sum of two numbers is: ' + sum);

let difference = x - y;
console.log('The difference of the two numbers is: ' + difference);

let product = x * y;
console.log('The product of the two numbers is: ' + product);

let quotient = x / y;
console.log('The quotient of the two numbers is: ' + quotient);

// let name = 'sumA'

// console.log(sum != difference);

function returnString() {
	return (sum > difference);
}

let sumA = returnString();
console.log('The sum is greater than the difference:' + ' ' + sumA);

function returnString1() {
	return (product > 0 && quotient > 0);
}

let productA = returnString1();
console.log('The product and quotient are both positive numbers: ' + productA);

function returnString2() {
	return(sum < 0 || difference < 0 || product < 0 || quotient < 0);
}

let differenceA = returnString2();
console.log('One of the results is negative: ' + differenceA);

function returnString3() {
	return(sum !== 0 && difference !== 0 && product !== 0 && quotient !== 0);
}

let quotientA = returnString3();
console.log('All the results are not equal to zero: ' + quotientA);
